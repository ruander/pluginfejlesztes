<?php
/**
 * @package Hgy_Swapi
 * @version 0.1
 */
/*
Plugin Name: George's Star Wars API sample file
Plugin URI: https://wordpress.org/plugins/swapi/
Description: Funny Star Wars character generator widget sample with shortcode functionality
Author: George Horvath
Version: 0.1
Author URI: https://iworkshop.hu/
Text Domain: hgyplugin
George's Star Wars API sample file is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
any later version.

George's Star Wars API sample file is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with George's Star Wars API sample file. If not, see {License URI}.
*/

function hgy_test()
{
    $string = 'hello, fut az első <b>plugin</b>unk!';
    echo $string;
}

//add_action('admin_notices', 'hgy_test');
//admin bezárható ütenetek
function hgy_swapi_admin_notice__success()
{
    ?>
    <div class="notice notice-success is-dismissible">
        <h3>Siker'</h3>
        <p><?php _e('Sikeres  adatmódosítás a swapi teszt bővitményben!', 'sample-text-domain'); ?></p>
    </div>
    <?php
}

//add_action('admin_notices', 'hgy_swapi_admin_notice__success');

function hgy_swapi_admin_notice__warning()
{
    ?>
    <div class="notice notice-warning is-dismissible">
        <h3>Ez az üzenet címe'</h3>
        <p><?php _e('Oooops!!!', 'sample-text-domain'); ?></p>
    </div>
    <?php
}

//add_action('admin_notices', 'hgy_swapi_admin_notice__warning');

function hgy_swapi_admin_notice__error()
{
    ?>
    <div class="notice notice-error is-dismissible">
        <h3>Ez az üzenet címe'</h3>
        <p><?php _e('Run, Run away, we are lost!!!', 'sample-text-domain'); ?></p>
    </div>
    <?php
}

//add_action('admin_notices', 'hgy_swapi_admin_notice__error');

//admin bar menü létrehozása
function toolbar_swapi_addon($wp_admin_bar)
{
    $args = array(
        'id' => 'hgy_swapi',
        'parent' => 'top-secondary',
        'title' => 'Hello admin bar'//icon
    ,
        //hover doboz:
        'meta' => array(
            'html' => '<div>test</div>',
            'class' => 'hgy-swapi'
        )
    );
    $wp_admin_bar->add_node($args);
}

add_action('admin_bar_menu', 'toolbar_swapi_addon');

//admin főmenü kiegészítése
add_action('admin_menu', 'swapi_admin_menu');

// Create WordPress admin menu
function swapi_admin_menu()
{

    $page_title = 'Swapi beállítások oldal';
    $menu_title = 'SWAPI';
    $capability = 'manage_options';
    $menu_slug = 'swapi-plugin';
    $function = 'swapi_info_page';
    $icon_url = 'dashicons-admin-generic';
    $position = 90;

    add_menu_page($page_title,
        $menu_title,
        $capability,
        $menu_slug,
        $function,
        $icon_url,
        $position);
    // Call update_extra_post_info function to update database
    add_action('admin_init', 'settingswapi_test');
}

// Create WordPress plugin page
function swapi_info_page()
{
    //$options = get_option('swapi_test');
    //print_r($options);
    ?>
    <h1>SWAPI beállítások</h1>
    <form method="post" action="options.php">
        <?php
        print_r(get_settings_errors('swapi_test'));
        settings_fields('swapi-settings'); ?>
        <?php do_settings_sections('swapi-settings'); ?>
        <table class="form-table">
            <tr valign="top">
                <th scope="row">SWAPI teszt adat:</th>
                <td><input type="text" name="swapi_test" value="<?php echo get_option('swapi_test'); ?>"/></td>
            </tr>
        </table>
        <?php submit_button(); ?>
    </form>
    <?php
    //print_r(ctype_alpha ( get_option('swapi_test')) );
}

// Create function to register plugin settings in the database
function settingswapi_test()
{
    register_setting('swapi-settings', 'swapi_test', 'validation_callback');       //

}

/*
 * adat ellenőrzés, ha nem jó, maradjon a régi
 */
function validation_callback($input)
{
    if (!ctype_alpha($input)) {
        add_settings_error('swapi_test', esc_attr('settings_updated'), __('Settings NOT saved.'), 'error');
        return get_option('swapi_test');
    }
    return $input;
}

//css file betöltése az admin felületre:
function load_custom_swapi_admin_style($hook)
{
    wp_enqueue_style('custom_wp_admin_css', plugins_url('css/styles.css', __FILE__));
}

//add_action('updated_option', 'test_callback', 10, 3);
add_action('admin_enqueue_scripts', 'load_custom_swapi_admin_style');

//Display admin notices
function swapi_notice()
{
    //get the current screen
    $updated = filter_input(INPUT_GET, 'settings-updated');
//print_r($updated);
    //if settings updated successfully
    if (empty(get_settings_errors('swapi_test')) && $updated) {

        ?>
        <div class="notice notice-success is-dismissible">
            <p><?php _e('Congratulations! data Saved.', 'hgyplugin') ?></p>
        </div>
    <?php } elseif (!empty(get_settings_errors('swapi_test'))) { ?>
        <div class="notice notice-error is-dismissible">
            <p><?php _e('Sorry, invalid data, Not Saved!', 'hgyplugin') ?></p>
        </div>
    <?php };
}

add_action('admin_notices', 'swapi_notice');
function my_plugin_load_plugin_textdomain()
{
    load_plugin_textdomain('hgyplugin', FALSE, basename(dirname(__FILE__)) . '/languages/');
}

add_action('plugins_loaded', 'my_plugin_load_plugin_textdomain');

//adjunk a footerhez valamit
function swapi_footer_addon()
{
    echo '<div style="text-align:center;background:rgba(0,0,0,0.65);color:lightblue;padding:5px;">We ❤ Star Wars!</div>';
}

add_action('wp_footer', 'swapi_footer_addon');
//Shortcode a plugin kimenetek megjelenítéséhez, ezt widgetbe (vagy bárhova) fogjuk elhelyezni
/**
 * Alkalmazás: [swapi]valami[/swapi], [swapi title="Cím"]
 * @param array $atts
 * @param null $content
 * @param string $tag
 * @return string
 */
function swapi_shortcode($atts = [], $content = null, $tag = '')
{
    // normalize attribute keys, lowercase
    $atts = array_change_key_case((array)$atts, CASE_LOWER);

    // override default attributes with user attributes
    $wporg_atts = shortcode_atts([
        'title' => 'Star Wars API teszt',
    ], $atts, $tag);

    // start output
    $o = '';

    // start box
    $o .= '<div class="swapi-box">';

    // title
    $o .= '<h2>' . esc_html__($wporg_atts['title'], 'swapi') . '</h2>';

    // enclosing tags
    if (!is_null($content)) {
        // secure output by executing the_content filter hook on $content
        $o .= apply_filters('the_content', $content);

        // run shortcode parser recursively
        $o .= do_shortcode($content);
    }

    // end box
    $o .= '</div>';

    // return output
    return $o;
}

function swapi_shortcodes_init()
{
    add_shortcode('swapi', 'swapi_shortcode');
}

add_action('init', 'swapi_shortcodes_init');

//Star wars api hívás kezelése
//@todo: jquery betöltése a sablonba (child-theme)
function swapi_get_json()
{
    $apiUrl = 'https://swapi.dev/api/';
    //$test = file_get_contents($apiUrl.'people/1/');

    echo '<script  src="https://code.jquery.com/jquery-3.5.1.min.js"
  integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0="  crossorigin="anonymous"></script>
 <script>
jQuery(document).ready(function($){
    
    //karakter név kattintás
    $(document).on("click",".swapi-box .charlist li", function(){
        let url = this.getAttribute("data-target").replace(/\/$/,\'\');//utolsó / eltávolítása
        let char = url.substring(url.lastIndexOf(\'/\')+1);//utolsó / utáni szám
        //let char = 1;
        getCharacter(char);
    })
    //vissza gomb
    $(document).on("click",".swapi-box .back", function(){
        getPeople();
    })
    //lapozó gombok
      $(document).on("click",".swapi-box .next", function(){
        let url = this.getAttribute("data-target");
        getPeople(url);
    })
      $(document).on("click",".swapi-box .prev", function(){
        let url = this.getAttribute("data-target");
        getPeople(url);
    })
    //összes karakter betöltése
    function getPeople(myUrl = null){
        console.log(myUrl);
        if(myUrl == null){
            myUrl = "'.$apiUrl.'" + "people/";
        }
        $.ajax({
            type:"GET",
            datatype: "json",
            url: myUrl,
            success: function(result){
                //console.log(result)
                let el = $(".swapi-box");
                let content = "<h3>SW karakterek</h3>" + 
                "<ul class=\"charlist\">";
                //result bejárása
                $.each(result.results, function(index,value){
                    content += "<li data-target=\"" + value.url + "\">" + value.name + "</li>";
                })
                content += "</ul>";
                //előző oldal gomb
                if(result.previous !== null){
                    content += "<button data-target=\"" + result.previous + "\" class=\"btn prev\">prev</button>"
                }
                //köv oldal gomb
                if(result.next !== null){
                    content += "<button data-target=\"" + result.next + "\" class=\"btn next\">next</button>"
                }
                //töltsük a dobozba a contentet
                el.html(content);
            },
            error: function(request,status,error){
                console.log(request,status,error)
            }
        });
    }
    getPeople();
    //egy karakter betöltése
    function getCharacter(char){
        let url = "'.$apiUrl.'" + "people/" + char + "/";
        getAjax(url);
    }
    function getAjax(url){//most karakternek
        
        $.ajax({
            type:"GET",
            datatype: "json",
            url: url,
            success: function(result){
                console.log(result)
                let el = $(".swapi-box");
                let content = "<h3>SW karakter</h3>" + 
                "<ul class=\"character\">";
                //result felbontása
                content += "<li><b>Név:</b> " + result.name + "</li>";
                content += "<li><b>Szem:</b> " + result.eye_color + "</li>";
                content += "<li><b>Magasság:</b> " + result.height + "</li>";
                content += "<li><b>Súly:</b> " + result.mass + "</li>";
                content += "<li><b>Nem:</b> " + result.gender + "</li>";
                
                content += "</ul>";
                //vissza gomb
                content += "<button class=\"btn back\">vissza</button>"
              
                //töltsük a dobozba a contentet
                el.html(content);
            },
            error: function(request,status,error){
                console.log(request,status,error)
            }
        });
    }
    //getCharacter(1);
    //vissza gomb
    
});
</script>';
}

add_action('wp_footer', 'swapi_get_json');
